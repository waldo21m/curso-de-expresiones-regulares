# Curso de Expresiones Regulares

Prof. Alberto Alcocer.

## Lecciones:

- [Introducción a las Expresiones Regulares](#module01)
  - [Lección #1 - Todo lo que aprenderás sobre expresiones regulares](#lesson01)
  - [Lección #2 - ¿Qué son las expresiones regulares?](#lesson02)
  - [Lección #3 - Aplicaciones de las expresiones regulares](#lesson03)
  - [Lección #4 - Introducción al lenguaje de expresiones regulares](#lesson04)
- [El lenguaje: caracteres, operadores y construcciones](#module02)
  - [Lección #5 - El caracter (.)](#lesson05)
  - [Lección #6 - Las clases predefinidas y construidas](#lesson06)
  - [Lección #7 - Los delimitadores +, *, ?](#lesson07)
  - [Lección #8 - Los contadores {1,4}](#lesson08)
  - [Lección #9 - El caso de (?) como delimitador](#lesson09)
  - [Lección #10 - Not (^), su uso y sus peligros](#lesson10)
  - [Lección #11 - Reto: Filtrando letras en números telefónicos utilizando negaciones](#lesson11)
  - [Lección #12 - Principio (^) y final de linea ($)](#lesson12)
- [Uso práctico de Expresiones Regulares](#module03)
  - [Lección #13 - Logs](#lesson13)
  - [Lección #14 - Teléfonos](#lesson14)
  - [Lección #15 - URLs](#lesson15)
  - [Lección #16 - Mails](#lesson16)
  - [Lección #17 - Localizaciones](#lesson17)
  - [Lección #18 - Nombres(?) Reto](#lesson18)
- [Usos avanzados en Expresiones Regulares](#module04)
  - [Lección #19 - Búsqueda y reemplazo](#lesson19)
- [Expresiones Regulares en lenguajes de programación](#module05)
  - [Lección #20 - Uso de REGEX para descomponer querys GET](#lesson20)
  - [Lección #21 - Explicación del proyecto](#lesson21)
  - [Lección #22 - Perl](#lesson22)
  - [Lección #23 - PHP](#lesson23)
  - [Lección #24 - Utilizando PHP en la práctica](#lesson24)
  - [Lección #25 - Python](#lesson25)
  - [Lección #26 - Java](#lesson26)
  - [Lección #27 - Java aplicado](#lesson27)
  - [Lección #28 - JavaScript](#lesson28)
  - [Lección #29 - grep y find desde consola](#lesson29)

## <a name="module01"></a> Introducción a las Expresiones Regulares

### <a name="lesson01"></a> Lección #1 - Todo lo que aprenderás sobre expresiones regulares

---

Este curso te va a enseñar qué son las expresiones regulares y cómo utilizarlas. Por ejemplo aplicaciones de búsqueda y filtrado, las expresiones regulares son extremadamente potentes, aprende a utilizarlas en este curso.

### <a name="lesson02"></a> Lección #2 - ¿Qué son las expresiones regulares?

---

Las expresiones regulares son patrones de caracteres que te permiten ir seleccionando o descartando datos en un archivo de texto como por ejemplo csv, o en una línea o un input, según coincidan o nó con este patrón.

Prácticamente todos los lenguajes de programación tienen librerías o módulos para manejar expresiones regulares.

Las expresiones regulares pueden ser muy complejas pero no son nada difíciles de entender.

A través de este curso, sin tecnicismos y con ejemplos puntuales, vamos a aprender a utilizarlas para que sean esa herramienta que siempre nos ayude, y sea la primera para solucionar problemas de grandes cantidades de datos en string.

*Lectura recomendada: https://www.youtube.com/watch?v=SDv2vOIFIj8*

### <a name="lesson03"></a> Lección #3 - Aplicaciones de las expresiones regulares

---

Buscar e investigar sobre Expresiones Regulares puede ser muy intimidante.

/^(.){5}\w?[a-Z|A-Z|0-9]$/ig

En serio pueden parecer muy, muy raras; pero la verdad es que no lo son.

En esta clase aprenderás, para qué te puede servir el saber usar bien las Expresiones Regulares; y es, en pocas palabras, para buscar.

### <a name="lesson04"></a> Lección #4 - Introducción al lenguaje de expresiones regulares

---

Con las expresiones regulares vamos a solucionar problemas reales, problemas del día a día.

¿Qué pasa si queremos buscar en un texto (txt, csv, log, cualquiera), todos los números de teléfonos que hay?

Tendríamos que considerar por ejemplo, que un teléfono de México serían 10 dígitos; hay quienes los separan con guión, hay quienes los separan con puntos, hay quienes no los separan sino que tienen los 10 dígitos exactos, y este patrón puede cambiar para otros países.

Esto mismo sucede con números de tarjetas de crédito, códigos postales, dirección de correos, formatos de fechas o montos, etc.

*Lectura recomendada: https://regex101.com/*

## <a name="module02"></a> El lenguaje: caracteres, operadores y construcciones

### <a name="lesson05"></a> Lección #5 - El caracter (.)

---

¿Qué es un archivo de texto, por ejemplo un CSV?

¿Qué es una cadena de caracteres?

Cada espacio en una cadena de texto se llena con un caracter, esto lo necesitamos tener perfectamente claro para comenzar a trabajar con expresiones regulares

Abriremos nuestro editor, qué en este curso recomendamos ATOM, vamos a presionar CTRL + F y podremos buscar por match idénticos.

**Notas:**

El punto (.) es igual a cualquier caracter. Cuando buscamos con Regex y colocamos ., automáticamente resaltará cada uno de los carácteres (Ojo: NO es la línea).

Ahora, si queremos buscar un caracter que tenga un espacio, colocamos `. `.

Otro ejemplo, si queremos conseguir 10 caracteres juntos, colocamos `..........`. Debemos estar mosca con los espacios o caracteres vacios porque también se cuentas en REGEX.

Ahora, si buscamos de 3 caracteres, puede ocurrir el escenario que haya múltiples de 3 y serán contabilizados. Si son 3 caracteres pegados con otros 3 y luego otros 2, solo contabilizará los 2 pares de 3 caracteres y obvia los otros 2 caracteres

*Lectura recomendada: https://elcodigoascii.com.ar/*

### <a name="lesson06"></a> Lección #6 - Las clases predefinidas y construidas

---

Las búsquedas en las expresiones regulares funcionan en múltiplos de la cantidad de caracteres que explícitamente indicamos.

**Notas:**

Para conseguir los digitos, usamos la siguiente clase: `\d`. La letra d significa digits.

Si queremos conseguir 3 dígitos seguidos, es similar al . de la lección anterior. Ejemplo: `\d\d\d`.

Para buscar una letra, usamos la siguiente clase: `\w`. La letra w significa word. *Importante: No reconoce caracteres especiales, como por ejemplo é*.

Para buscar un espacio en blanco, usamos la siguiente clase: `\s`. La letra s significa white spaces. *Importante: No reconoce el retorno del carro o el enter*.

Otra forma de escribir nuestras clases es usando los corchetes. Si escribimos `[0-9]` es el equivalente a usar `\d`. La ventaja de usar esto es que podemos hacer búsquedas más puntuales. Por ejemplo, podemos buscar en un rango del `[6-9]`.

También podemos aplicarlo con los caracteres, como `[a-z]`. Esto solo busca de un rango de la "a" a la "z" pero solo minúsculas. Si queremos minúsculas y mayúsculas, sería `[a-zA-Z]`. Y si queremos que sea parecido a la `\w`, sería `[a-zA-Z0-9]`.

Otro ejemplo sería el siguiente: `[a-fA-F0-9_\.]`. Lo que significa esta expresión regular es *Busca un caracter que sea entre la "a" hasta la "f" que sea mayúscula o minúsculas o un dígito comprendido entre el 0 y el 9 o el underscore o el punto*. No confundir el punto con la clase del punto por que lo estamos escapando con el slash `\.`.

`[a-fA-F0-9_\.]` es igual a `[a-fABCDEF0-9_\.]`.

### <a name="lesson07"></a> Lección #7 - Los delimitadores +, *, ?

---

**Hoja rápida de consulta:**

* \w - Caracteres de palabras.
* \d - Dígitos.
* \s - Espacios/invisibles en blanco.
* [0-9] ~ \d.
* [0-9a-zA-Z_] ~ \w.
* \* Greedy - todo.
* \+ Uno o más.
* ? Cero a uno.

**Delimitadores:**

El (*) coincide con el carácter o grupo de caracteres anteriores de la expresión regular de cero o más veces. Ejemplo: `\d*`. Esto quiere decir que seleccionará un conjunto de dígitos que exista o no exista.

El (+) coincide con el carácter o grupo de caracteres anteriores de la expresión regular una o más veces. Ejemplo: `\d+`. Esto quiere decir que seleccionará un conjunto de dígitos que exista.

El (?) coincide con el carácter o grupo de caracteres anteriores de la expresión regular cero o una vez. Ejemplo: `\d?`. Esto quiere decir que puede o no existir un dígito.

**Importante: Es bueno buscar por expresiones regulares y reemplazar por un guión (-) para ver cual es el resultado final.**

Si queremos buscar una serie de dígitos que termine con una letra, se escribiría de la siguiente manera: `\d+[a-zA-Z]`, pero también podemos escribir que pueda tener o no dígitos de forma previa, pero que termine en una letra y es de la siguiente manera: `\d*[a-zA-Z]`.

### <a name="lesson08"></a> Lección #8 - Los contadores {1,4}

---

Lo que vamos a aprender en esta clase es comenzar a generalizar nuestras búsquedas, a ser específicos cubriendo grandes cantidades de caracteres sin tener que escribir de forma repetitiva como sería poner por ejemplo “\d\d\d\d\d\d\d\d…”.

**Notas:**

Para buscar una cantidad de dígitos lo hacemos de la siguiente manera: `\d{2}`.

Si queremos establecer un rango, sería `\d{2,2}`, es decir, mínimo 2 y máximo 2. Es mejor hacerlo de la segunda manera. Si queremos que sea desde un número al infinito, seria: `\d{2,}`.

Para el ejemplo de los teléfonos, definimos la siguiente expresión regular:

`\d{2,2}[-\. ]?\d{2,2}[-\. ]?\d{2,2}[-\. ]?\d{2,2}[-\. ]?\d{2,2}[-\. ]?`

Lo que hace, es agarrar 2 dígitos y que puede tener de forma seguida o no un guión, un punto o un espacio en blanco. Si lo colocamos 5 veces, tenemos el regex de un número celular de México.

También podemos colocar delimitadores en nuestras clases personalizadas. Ejemplo:

`\d{2,2}[-\. ]{0,1}\d{2,2}[-\. ]{0,1}\d{2,2}[-\. ]{0,1}\d{2,2}[-\. ]{0,1}\d{2,2}[-\. ]{0,1}`

Pero esto es lo mismo que usar el ?.

### <a name="lesson09"></a> Lección #9 - El caso de (?) como delimitador

---

El ? indica al patrón que encuentre las coincidencias de manera rápida (o greedy); es decir, devolviendo el resultado más pequeño que haga match hasta donde se encuentra el delimitador, y esto lo haga tantas veces como sea posible dentro de la cadena.

**Notas:**

Ejemplo: `.+?[,\n]`.

El signo de interrogación tiene dos usos, el primero es para encontrar o no si existe la condición y el otro uso es para delimitarlo o achicarlo, es decir, que no te agarre toda la línea sino uno por uno. Esto es muy útil para los csv.

### <a name="lesson10"></a> Lección #10 - Not (^), su uso y sus peligros

---

Este caracter nos permite negar una clase o construir “anticlases”, vamos a llamarlo así, que es: toda la serie de caracteres que no queremos que entren en nuestro resultado de búsqueda.

Para esto definimos una clase, por ejemplo: [ 0-9 ], y la negamos [ ^0-9 ] para buscar todos los caracteres que coincidan con cualquier caracter que no sea 0,1,2,3,4,5,6,7,8 ó 9.

**Notas:**

* Con \D podemos buscar todo lo que no sea números.
* Con \W buscamos todo lo que no sea strings.
* Con \S buscamos todo lo que no sea espacios en blancos.

Para haer una anticlase lo hacemos de la siguiente manera: `[^0-5a-c]`. Esto significa que escogerá todos los valores que no estén comprendidos entre el 0 al 5 o la a, b o c.

### <a name="lesson11"></a> Lección #11 - Reto: Filtrando letras en números telefónicos utilizando negaciones

---

En el texto siguiente:

555658

56-58-11

56.58.11

56.78-98

65 09 87

76y87r98

Definir un patrón que haga match a todas las líneas excepto a la la última, la que tiene letras.

Es decir, seleccionar todas sin importar el caracter de separación, excepto cuando los números están separados entre sí por letras.

**Respuesta:**

`(\d{2,2}\W?){3,3}`

Con los paréntesis, podemos encerrar una sentencia e indicar un contador para no copiar y pegar como en lecciones anteriores.

### <a name="lesson12"></a> Lección #12 - Principio (^) y final de linea ($)

---

Estos dos caracteres indican en qué posición de la línea debe hacerse la búsqueda:
* El ^ se utiliza para indicar el principio de línea.
* El $ se utiliza para indicar final de línea.

^ ------------- $

**Notas:**

Un ejemplo de esta lección, sería la siguiente: `^\d{3,6}$`. Lo que indica es que agarrará toda la línea completa que tenga entre 3 a 6 dígitos. Si tiene más o menos, no los tomará en cuenta.

Otro ejemplo: `^[^\d].*$`. Escoge todas las líneas que no tengan de forma previa un dígito.

Otro ejemplo útil sería en los csv. Para este caso si queremos que sea de 3 columnas como en el archivo csv.txt, debe ser con la siguiente sentencia: `^\w+,\w+,\w+$`.

## <a name="module03"></a> Uso práctico de Expresiones Regulares

### <a name="lesson13"></a> Lección #13 - Logs

---

Las expresiones regulares son muy útiles para encontrar líneas específicas que nos dicen algo muy puntual dentro de los archivos de logs que pueden llegar a tener millones de líneas.

**NOTAS:**

Para hayar todos los warnings de los logs de liners.txt, lo haremos de la siguiente manera:

`^\[LOG.*\[WARN\].*$`.

Otro caso, si queremos conseguir todos los logs de un usuario en específico, lo hacemos de la siguiente manera:

`^\[LOG.*\[LOG\].*user:@beco\] .*$`

O si queremos cualquier usuario:

`^\[LOG.*\[LOG\].*user:@\w+?\] .*$`

### <a name="lesson14"></a> Lección #14 - Teléfonos

---

Resolución de este ejercicio:

`^\+?\d{2,3}[^\da-zA-Z]?\d{2,3}[^\da-zA-Z]?\d{2,3}[#pe]?\d*$`

Otra forma de simplificarlo es la siguiente:

`^\+?(\d{2,3}\W?){2,2}\d{2,3}[#pe]?\d*$`

### <a name="lesson15"></a> Lección #15 - URLs

---

Una de las cosas que más vamos a usar en la vida, seamos frontend o backend, serán directamente dominios o direcciones de internet; ya sea direcciones completas de archivo (una url) o puntualmente dominios para ver si es correcto un mail o no.

**Notas:**

Resolución de este ejercicio:

`https?:\/\/[\w\-\.]+\.\w{2,6}\/?\S*$`

### <a name="lesson16"></a> Lección #16 - Mails

---

Quedamos en que ya podemos definir URLs, y dentro de las URLs están los dominios. No es infalible, pero es muy útil para detectar la gran mayoría de errores que cometen los usuarios al escribir sus emails.

**Notas:**

Resolución del ejercicio:

`[\w\._]{5,30}\+?[\w]{0,10}@[\w\.\-]{3,}\.\w{2,5}`

### <a name="lesson17"></a> Lección #17 - Localizaciones

---

Esta clase nos va a servir para ver unos tips comunes de qué hacer y sobre todo qué no hacer con expresiones regulares, usando como ejemplo datos de posicionamiento en el mapa: latitud y longitud.

**Notas:**

Resolución del primer ejercicio:

`^-?\d{1,3}\.\d{1,6},\s?-?\d{1,3}\.\d{1,6},.*$`

Resolución del segundo ejercicio:

`^-?\d{1,3}\s\d{1,2}' \d{1,2}\.\d{2,2}"[WE],\s?-?\d{1,3}\s\d{1,2}' \d{1,2}\.\d{2,2}"[SN]$`

*Importante: Las expresiones regulares no hace validaciones si los valores están bien escritos o no, eso le corresponde al lenguaje de programación. Solo se encarga de que la forma esté bien escrita.*

https://what3words.com/pretty.needed.chill?redirect=true

Resolución del ejercicio what3words:

`[a-z]{3,}\.[a-z]{3,}\.[a-z][a-z][a-z]+`

### <a name="lesson18"></a> Lección #18 - Nombres(?) Reto

---

Resolución del ejercicio:

`^[A-Z][a-z]{3,}\s?[A-Z]?[a-z]{0,}$`

Pero es muy importante saber que esta expresión regular no almacena caracteres especiales como acentos o la letra ñ. Eso debería expandirse dependiendo de la necesidad del sistema.

## <a name="module04"></a> Usos avanzados en Expresiones Regulares

### <a name="lesson19"></a> Lección #19 - Búsqueda y reemplazo

---

Al igual que una navaja suiza, las expresiones regulares son una herramienta increíblemente útil pero tienes que darle la importancia y las responsabilidades adecuadas a cada una, ya que no son la panacea, no solucionan todos los problemas.

El uso más conveniente de las expresiones regulares es buscar coincidencias o matches de cadenas en un texto, y si es necesario, reemplazarlas con un texto diferente.

**Notas:**

La resolución parcial es la siguiente:

`^\d+::([\w\s:,\(\)'\.\-&!\/]+)\s\((\d\d\d\d)\)::.*$`

La verdadera magia de buscar, agrupar y reemplazar es que cuando tipeamos: `$1,$2`. Lo que hace es dejar únicamente los dos grupos que habíamos definido en nuestra expresión regular y así podemos ver de forma más fácil cuales son los que no están faltando.

Lo importante de esto es que el reemplazo fue en cuestión de milisegundos y sería muchísimo más rápido en cualquier lenguaje de programación.

También podemos construir queries de SQL llevando ese listado a una sentencia de base de datos de la siguiente manera:

REGEX: `^\d+::([\w\s:,\(\)'\.\-&!\/]+)\s\((\d\d\d\d)\)::.*$`
Reemplazar: `INSERT INTO movies (year, title) values($2, '$1')`

También podemos construir objetos JSON de la siguiente manera:

REGEX: `^\d+::([\w\s:,\(\)'\.\-&!\/]+)\s\((\d\d\d\d)\)::.*$`
Reemplazar: `{title: "$1", year: $2},`

También podemos eliminar datos basura. Si asumimos las películas que no se reemplazaron como data basura, podemos buscarlo con una expresión regular y reemplazarla por un string vacío por ejemplo.

## <a name="module05"></a> Expresiones Regulares en lenguajes de programación

### <a name="lesson20"></a> Lección #20 - Uso de REGEX para descomponer querys GET

---

Al hacer consultas a sitios web mediante el método GET se envían todas las variables al servidor a través de la misma URL.

La parte de esta url que viene luego del signo de interrogación ? se le llama query del request que es: `variable1=valor1&variable2=valor2&...` y así tantas veces como se necesite. En esta clase veremos como extraer estas variables usando expresiones regulares.

**Notas:**

REGEX: `[\?&](\w+)=([^&\n]+)`
Reemplazar: `\n - $1=$2`

Con esto, podemos ver cada una de las queries de una ruta GET.

### <a name="lesson21"></a> Lección #21 - Explicación del proyecto

---

Vamos a utilizar un archivo de resultados de partidos de fútbol histórico con varios datos. El archivo es un csv de más de 39000 líneas diferentes.

Con cada lenguaje intentaremos hacer una solución un poquito diferente para aprovecharlo y saber cómo utilizar expresiones regulares en cada uno de los lenguajes.

Usaremos las expresiones regulares en:
* Perl
* PHP
* Python
* Javascript

### <a name="lesson22"></a> Lección #22 - Perl

---

Explicación de REGEX en Perl.

```perl
#!/usr/bin/perl

use strict;
use warnings;

my $t = time;

open(my $f, "<../../files/results.csv") ordie("no hay archivo");

my $match = 0;
my $nomatch = 0;

while(<$f>) {
	chomp;
	# 2018-06-04,Italy,Netherlands,1,1,Friendly,Turin,Italy,FALSE
	if(m/^([\d]{4,4})\-.*?,(.*?),(.*?),(\d+),(\d+),.*$/){
		if($5 > $4) {
			printf("%s: %s (%d) - (%d) %s\n",
				$1, $2, $4, $5, $3
			);
		}
		$match++;
	} else {
		$nomatch++;
	}
}

close($f);

printf("Se encontraron \n - %d matches\n - %d no matches\ntardo %d segundos\n"
	, $match, $nomatch, time() - $t);
printf("VISITANTES GANADORES\n");
```

### <a name="lesson23"></a> Lección #23 - PHP

---

Código de la lección:

```php
<?php
$file = fopen("../files/results.csv","r");

$match   = 0;
$nomatch = 0;

while(!feof($file)) {
    $line = fgets($file);
    if(preg_match(
        '/^2018\-01\-(\d\d),.*$/',
        $line,
        $m
      )
    ) {
        print_r($m);
        $match++;
    } else {
        $nomatch++;
    }
}
fclose($file);

printf("\n\nmatch: %d\nnomatch: %d\n", $match, $nomatch);
```

### <a name="lesson24"></a> Lección #24 - Utilizando PHP en la práctica

---

Código de la lección:

```php
<?php
$file = fopen("../files/results.csv","r");

$match   = 0;
$nomatch = 0;

$t = time();

while(!feof($file)) {
    $line = fgets($file);
    if(preg_match(
        '/^(\d{4}\-\d\d\-\d\d),(\w+),(\w+),(\d+),(\d+),.*$/i',
        $line,
        $m
      )
    ) {
        if ($m[4] == $m[5]) {
            printf("empate: ");
        } elseif ($m[4] > $m[5]) {
            echo "local:  ";
        } else {
            echo "visitante: ";
        }
        printf("\t%s, %s [%d-%d]\n", $m[2], $m[3], $m[4], $m[5]);
        $match++;
    } else {
        $nomatch++;
    }
}
fclose($file);

printf("\n\nmatch: %d\nnomatch: %d\n", $match, $nomatch);

printf("tiempo: %d\n", time() - $t);
```

### <a name="lesson25"></a> Lección #25 - Python

---

Código de la lección:

```python
import re

pattern = re.compile(r'^([\d]{4,4})\-\d\d\-\d\d,(.+),(.+),(\d+),(\d+),.*$')

f = open("./results.csv", "r", encoding="utf8")

for line in f:
	res = re.match(pattern, line)
	if res:
		total = int(res.group(4)) + int(res.group(5))
		if total > 10:
			print("Goles: {}, {} {},{} [{}-{}]".format(total, res.group(1), res.group(2), res.group(3),res.group(4), res.group(5)))

f.close()
```

### <a name="lesson26"></a> Lección #26 - Java

---

Código de la lección:

```java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidObjectException;

public class regex {
  publicstaticvoid main(String[] args) {
    String file = "results.csv";

    try {
      BufferedReader br = newBufferedReader(new FileReader(file));
      Stringline;

      while((line = br.readLine()) != null) {
        System.out.println(line);
      }

      br.close();
    } catch(IOException err) {
      System.out.println(err);
    }
  }
}
```

### <a name="lesson27"></a> Lección #27 - Java aplicado

---

Código de la lección:

```java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class java_regex {
  public static void main(String[] args) {
    final String file = "results.csv";
    final Pattern pat = Pattern.compile("^[\\d\\-]+,.*[zk].*$");
    
    try {
      BufferedReader br = new BufferedReader(new FileReader(file));
      String line;

      while((line = br.readLine()) != null) {
        Matcher matcher = pat.matcher(line);
        
        if(matcher.find()) {
          System.out.println(line);
        }
      }

      br.close();
    } catch(IOException err) {
      System.out.println(err);
    }
  }
}
```

### <a name="lesson28"></a> Lección #28 - JavaScript

---

Código de la lección:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Js Regex</title>

  <style>
    body {
      font-family: Arial;
    }

    input {
      font-size: 18px;
      padding: 6px;
    }

    .btn {
      cursor: pointer;
      border: none;
      border-radius: 10px;
      padding: 6px 14px;
      font-size: 18px;
      color: white;
      background-color: #43c579;
      border-bottom: 5px solid #35b46a;
      box-shadow: 3px 3px 10px 0 rgba(65, 65, 65, 0.849);
      margin-left: 18px;
      transition: 300ms;
    }

    .btn:hover {
      background-color: #3ab36c;
      border-bottom: 5px solid #289b58;
      transform: scale(1.1);
    }

    .disabled {
      background-color: #c54343;
      border-bottom: 5px solid #b43535;
    }

    .disabled:hover {
      background-color: #b33a3a;
      border-bottom: 5px solid #9b2828;
    }
  </style>
</head>
<body>
  <input type="text" id="email" placeholder="Your Email" onkeyup="validate(this.value)" autofocus>
  <input type="button" id="button" value="submit" class="btn disabled" onclick="correct()" disabled>
  
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
    const button = document.getElementById('button')

    function validate(email) {
      if(email.match(/^[^@]{5,10}@[\w\.]{2,10}\.\w{2,5}$/i)) {
        button.disabled = false
        button.classList.remove('disabled')
      } else {
        button.disabled = true
        button.classList.add('disabled')
      }
    }

    function correct() {
      swal({
        title: 'Congratulations!',
        text: 'This is a correct Email',
        icon: 'success',
        button: 'Thank\'s'
      })
    }
  </script>
</body>
</html>
```

**Importante: {1,} este contador no es permitido.**

### <a name="lesson29"></a> Lección #29 - grep y find desde consola

---

En los sistemas operativos basados en UNIX podemos utilizar expresiones regulares a través de la consola mediante los comandos grep y find.
* **grep**: Nos ayuda a buscar dentro de los archivos, textos muy puntuales utilizando una versión muy reducida de expresiones regulares.
* **find**: Se utiliza para encontrar archivos en un determinado directorio a partir de diversas reglas de búsqueda que incluyen expresiones regulares.


**Notas:**

Con el comando: `cat results.csv | wc -l`, podemos contar la cantidad de registros que tiene el csv.

Con el comando `cat results.csv | grep ^2012`, ejecutamos una expresión regular. Otro ejemplo: `cat results.csv | grep ,3\[0-9\],`.

Para buscar todos los juegos que sean false, sería: `cat results.csv | grep \[SE\]`, pero esto también hallaría otros strings que contengan 'SE'.

Para conseguir los juegos de Brazil y Uruguay en el año 1950, sería: `cat results.csv | grep Brazil | grep Uruguay | grep ^1950`.
